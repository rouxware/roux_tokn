
/*
	Abstract:	Example parser using the tokenising modules.

				This example is more than just a toy programme invoking silly examples
				of the tokenisers. While it is NOT a complete json parser, it will
				parse the first complete "outer block" in the input file and return
				a hash for the calling programme to use to retrieve values. It will
				not correctly handle two "back to back" objects or arrays; the first
				will be correctly parsed and returned, but any subsequent ones will not
				be parsed.

				The basic and multi tokenisers from this crate are used to illustrate
				how they can be used to build a more complex parser than just splitting
				comma separated tokens.
				
	Date:		15 April 2021
	Author:		E. Scott Daniels
*/


use std::fmt;
use std::io::prelude::*;
use std::fs::File;
use roux_tokn::multi as tkm;
use roux_tokn::basic as tkb;

// -----------------------------------------------------------------------------------

#[ derive( PartialEq ) ]
enum State {
	BuildObject,		// "object" in progress (set when { is encountered)
	BuildArray,			// array collection inprogress (set when [ is encountered)
	NeedValue,			// inside of an array, or immediately following a string:
	None				// initial state
}

/*
	Allows state to be passed directly to a print/write function.
*/
impl std::fmt::Display for State {
	fn fmt( &self, f: &mut fmt::Formatter<'_> ) -> fmt::Result {
		match self {
			State::BuildObject => write!( f, "build-obj" ),
			State::BuildArray => write!( f, "build-array" ),
			State::NeedValue => write!( f, "need-val" ),
			State::None => write!( f, "no-state" )
		}
	}
}

	
// -----------------------------------------------------------------------------------

/*
	Given a string convert to float without the hastle of "exceptions" for the caller. 
	C got this right; bad number is just 0, and if you don't agree, keep it to your self.
*/
fn atof64( what: &str ) -> f64 {
    let rval = what.parse::<f64>();

    match rval {
        Ok( v ) => return v,
        _ => return 0.0,
    }
}

// -----------------------------------------------------------------------------------


/*
	token types
*/
#[ derive(PartialEq, Copy, Clone) ] 
enum TType {
	Nil,
	Number,
	String,
	OpenCurly,
	CloseCurly,
	OpenSq,
	CloseSq,
	Comma,
	Colon,
	WhiteSpace
}

/*
	Allows token type enum to be given directly on write/print statements.
*/
impl std::fmt::Display for TType {
	fn fmt( &self, f: &mut fmt::Formatter<'_> ) -> fmt::Result {
		match self {
			TType::Nil => write!( f, "nil" ),
			TType::Number => write!( f,  "number" ),
			TType::String => write!( f,  "string" ),
			TType::OpenCurly => write!( f,  "open-curly" ),
			TType::CloseCurly => write!( f,  "close-curly" ),
			TType::OpenSq => write!( f,  "open-sq" ),
			TType::CloseSq => write!( f,  "close-sq" ),
			TType::Comma => write!( f,  "comma" ),
			TType::Colon => write!( f,  "colon" ),
			TType::WhiteSpace => write!( f,  "whitespace" )
		}	
	}
}

// -----------------------------------------------------------------------------------

/*
	A set of token types that are valid "next" and thus are "expected"
	A hash might be more efficent if the set grows beyond a few types.
*/
struct Expect {
	expected: Vec< TType >
}

impl Expect {
	fn mk( what: Vec< TType > ) -> Expect {
		return Expect {
			expected: what
		};
	}

	/*
		Given a token type, return true if that type is in the set.
	*/
	fn is_expected( &self, what: &TType ) -> bool {
		for t in &self.expected {
			if t == what {
				return true;
			}
		}

		return false;
	}
}

/*
	Vector doesn't implement clone/copy so we must.
*/
impl Clone for Expect {
	fn clone( &self ) -> Expect {
		let mut nv: Vec< TType > = Vec::new();
		for t in &self.expected {
			nv.push( *t );
		}

		return Expect {
			expected: nv	
		};
	}
}

// -----------------------------------------------------------------------------------
/*
	A name path represents a unique reference to a value. "Nodes" are
	separated by dots (.) with the root string being supplied when
	the path is created. For the parser, the json below would
	generate the following name paths assuming that the root string
	was given as "[r]":

		[r]author.first
		[r]author.last
		[r]books
	
		{
			"author": {
				"first": "Morris", "last": "Eckhouse"
			},
			"books": [
				"Legends of the Tribe", 
				"Bob Feller",
				"This date in Pittsburgh Pirates history"
			]
		}
*/
struct Path {
	path:	Vec< String >,
	sep:	String
}

impl Path {
	fn mk( root: &str ) -> Path {
		let mut nv: Vec< String > = Vec::new();
		nv.push( String::from( root ) );
		return Path {
			path:	nv,
			sep:	String::from( "." )
		}
	}

	fn set_sep( &mut self, sep: &str ) {
		self.sep = String::from( sep );
	}

	fn push( &mut self, leaf: String ) {
		self.path.push( leaf );	
	}

	fn pop( &mut self ) {
		if self.path.len() > 1 {
			self.path.pop();
		}
	}

	fn to_str( &self ) -> String {
		let mut pstr: String = String::from( "" );			// path string

		for i in 0..self.path.len() {
			if i > 0 {
				pstr.push_str( &self.sep )
			}
			pstr.push_str( &self.path[i] );
		}

		return pstr;
	}
}

impl std::fmt::Display for Path {
	fn fmt( &self, f: &mut fmt::Formatter<'_> ) -> fmt::Result {
		write!( f, "{}", self.to_str() )
	}
}

// -----------------------------------------------------------------------------------

/*
	A lump of crap loosly bound together after parsing the json input.
*/
struct Jlump {
	hash:	std::collections::HashMap< String, Thing >,		// refernce to things by name
}

impl Jlump {
	fn mk( ) -> Jlump {
		return Jlump {
			hash: std::collections::HashMap::new()
		};
	}

	/*
		Given a name, return the thing or none.
	*/
	fn get_thing( &self, name: &str ) -> Thing {
		return match  self.hash.get( name ) {
			Some( t ) => t.clone(),
			_ => Thing::None
		}
	}

	/*
		Given a name and index, find the array and return
		the thing at that index. If the name isn't an array,
		or index is out of bounds, returns a none thing.
	*/
	fn get_ele( &self, name: &str, idx: usize ) -> Thing {
		let t = match self.get_thing( name ) {
			Thing::Array( a ) => a, 
			_ => return Thing::None
		};

		println!( "get_ele: {} has {} elements", name, t.len() );
		return t.get( idx );
	}

	//fn find_ele_with( &self, ele_name: &str, with

	/*
		Put a thing into the lump using name as the hash.
	*/
	fn add_thing( &mut self, name: &str, thing: Thing ) {
		self.hash.insert( String::from( name ), thing );
	}

	fn dump_keys( &self ) {
		for kvpair in &self.hash {
			println!( "{}", kvpair.0 );
		}
	}
}

// -----------------------------------------------------------------------------------

#[ derive( Clone ) ]
enum Thing {
	Float( f64 ),
	Str( String ),
	Array( Arraything ),
	Object( String ),			// string is the root path to the object
	None
}

impl std::fmt::Display for Thing {
	fn fmt( &self, f: &mut fmt::Formatter<'_> ) -> fmt::Result {
		match self {
			Thing::Float(fv) =>  write!( f, "{}", fv ),
			Thing::Str( sv ) =>  write!( f, "{}", sv ),
			Thing::Array( av ) => write!( f, "{}", av ),
			Thing::Object( ov ) => write!( f, "obj: {}", ov ),
			Thing::None => write!( f, "none" )
		}
	}
}

/*
	A wrapper on a vector that allows us to implement clone and display
	as well as to provide a get which clones the element rather than
	giving a reference.
*/
struct Arraything {
	elements: Vec< Thing >
}

impl Arraything {
	fn mk( ) -> Arraything {
		let nv: Vec< Thing > = Vec::new();
		return Arraything {
			elements: nv
		}
	}
	
	fn len( &self ) -> i32 {
		return self.elements.len( ) as i32;
	}

	fn push( &mut self, t: Thing ) {
		self.elements.push( t );
	}	

	fn get( &self, idx: usize ) -> Thing  {
		if idx >= self.elements.len() {		// usize, so no need to check lower bound
			return Thing::None;
		}

		return self.elements[idx].clone();
	}

	/*
		Return the index of the next pushe'd element.
	*/
	fn pidx( &self ) -> usize {
		return self.elements.len();
	}

	/*
		Return the index of the next element as a string.
	*/
	fn pidx_str( &self ) -> String {
		return format!( "{}",  self.pidx() );
	}
}

/*
	Vector doesn't implement clone/copy so we must.
*/
impl Clone for Arraything {
	fn clone( &self ) -> Arraything {
		let mut nv: Vec< Thing > = Vec::new();
		for ele in &self.elements {
			nv.push( ele.clone() );
		}

		return Arraything {
			elements: nv	
		};
	}
}

impl std::fmt::Display for Arraything {
	fn fmt( &self, f: &mut fmt::Formatter<'_> ) -> fmt::Result {
		write!( f, "[" )?;
		for i in 0..(self.elements.len()-1) {
			write!( f, "{}, ", self.elements[i]  )?;
		}	

		let i = self.elements.len() - 1;
		write!( f, "{} ", self.elements[i]  )?;
		writeln!( f, "]" )
	}
}



// -- utility things -----------------------------------------------------------------

/*
    C-like atoi function that returns 0 for an 'invalid' string rather than core
    dumping all over the place.
*/
pub fn atoi32( what: &str ) -> i32 {
	let rval = what.parse::<f64>();

	match rval {
		Ok( v ) => return v as i32,
		_ => return 0,
	}
}
	
/*
	Read the next chunk of junk from a file.
*/
fn read_chunk( f: &mut std::fs::File, buf: &mut[u8] ) -> i32 {
	let nread = match f.read( &mut buf[..] ) {
		Ok( n ) => n as i32,
		Err( e ) => {
			eprintln!( "unable to read from file: {}", e );
			-1 as i32
		}
	};

	return  nread
}

/*
	Return true if the string contains a vaild number.
*/
fn is_number( s: &str ) -> bool {
	if s.len() < 1 {
		return false;
	}	
	let mut start: usize = 0;
	if &s[0..1] == "+" || &s[0..1] == "-" {
		start = 1
	}
	if s[start..].chars().all( char::is_numeric ) {
		return true;
	}

	let tokens = tkb::tokenise( &s[start..], "." );
	if tokens.len() != 2 {
		return false;
	}

	if tokens[0].chars().all( char::is_numeric ) {
		return tokens[1].chars().all( char::is_numeric );
	}
	
	return false;
}

/*
	State stack pushes are wrapped as we never push need value
	and this helps to keep the code cleaner by enforcing it here.
*/
fn push_sstack( state: State, stack: &mut Vec< State > ) {
	if state != State::NeedValue {
		stack.push( state );
	}
}

/*
	Stack poppers ensure that we get something valid back.
*/
fn pop_stack( stack: &mut Vec< State > ) -> State {
	return match stack.pop() {
		Some( s ) => s,
		_ => State::None
	};
}


/*
	Pop an array from the given stack. If there is nothing to pop
	returns an empty new one.
*/
fn pop_astack( stack: &mut Vec< Arraything > ) -> Arraything {
	return match stack.pop() {
		Some( a ) => a,
		_ => Arraything::mk()
	};
}

/*
	Look at the token and return its type.
*/
fn type_token( s: &str ) -> TType {
	if s.len() == 1 {
		return match s {
			"{" => TType::OpenCurly,
			"}" => TType::CloseCurly,
			"[" => TType::OpenSq,
			"]" => TType::CloseSq,
			"," => TType::Comma,
			":" => TType::Colon,
			"\n" | "\t" | " "  => TType::WhiteSpace,
			"0" | "1" | "2" | "3" | "4" | "5" | "6" | "7" | "8" | "9" => TType::Number,
			_ => TType::String
		};
	}

	if s == "\\nil" {			// likely a empty quoteed str in the input
		return TType::Nil;
	}

	if is_number( &s ) {
		return TType::Number;
	}

	return TType::String;
}

/*
	Given a token return true if it's a sep.  We need to know this when
	wondering if the last token has been split across two reads. Seps should
	all be one character and thus don't need to be combined with the next
	read.
*/
fn is_sep( s: &str ) -> bool {
	if s.len() == 1 {
		return match s {
			"{" =>  true,
			"}" =>  true,
			"[" =>  true,
			"]" =>  true,
			"," =>  true,
			":" =>  true,
			"\n" | "\t" | " "  => true,
			_ => false
		};
	}

	return false
}

/*
	Read chunks from the input file (8K) and parse. It is possible that the final
	token of the chunk will be split:
		- quoted token close quote not found
		- partial digits from number
		- partial unquoted token

	To handle these, we process all but the last token in the chunk, and push the
	last token onto the front of the next chunk before parsing. We need to handle
	certain edge cases when we do that:
		if quote in progress, token is a number or token is a separator, then
		the token can be pushed as is

		if token is string, then we must quote the token so that any separator characters
		which were embedded will be preserved.  For example, if the final token was
		"Marching 110" pushing it without quotes would result in the return of two
		tokens: "Marching" and "110"  with 110 looking like a number.  	

	Tokens are examined for type and depending on what is expected they are added to
	the key/value hash. 
*/
fn parse( mut f: std::fs::File ) -> Jlump {
	let mut buf: [u8; 8192] = [0; 8192];				// 8k input buffer
	let mut sstack: Vec< State > = Vec::new();			// statk of states
	let mut astack: Vec< Arraything > = Vec::new();	// stack of array things for nested arrays
	//let mut ostack: Vec< String > = Vec::new();	// object's field names as an array

	let mut jl = Jlump::mk( );
	let mut at = Arraything::mk();						// must always have an array open to keep compiler from complaining

	let mut next = Expect::mk( vec![ TType::OpenSq, TType::OpenCurly ] );			// initially only two things expected

	let mut partial_tok = Option::None;						// no partial_tok token to start

	let mut dq = false;										// dangling quote -- set to true if the last parsed out token was quoted and not complete
	let mut state = State::None;							// current parse state
	let mut path = Path::mk( "[root]" );					// current name path
	path.set_sep( "." );

	let mut dt: String;										// dangling token (last token in input which might be incomplete; scope beyond loop
	loop {
		let nread = read_chunk( &mut f, &mut buf[..] );
		if nread <= 0 {		// last token should be \n (whitespace) so we should have parsed the closing bracket
			break;
		}

		let chunk = match std::str::from_utf8( &buf[0..nread as usize] ) {
			Ok( s ) => s,
			Err( e ) => {
				eprintln!( "chunk conversion error: {}", e );
				break;
			}
		};

		let pbuf = match partial_tok {			// if we have a potential partial token, add it to the front of the buffer
			Some( t ) => {
				if dq || is_number( t ) || is_sep( t ) {		// in a quote or a number, just push
					let mut tok = String::from( t );
					tok.push_str( chunk );
					tok
				} else {
					let mut tok = String::from( "\"" );		// quote a string token as it might have embedded seps
					tok.push_str( t );
					tok.push( '"' );
					tok.push_str( chunk );
					tok
				}
			},
			None => String::from( chunk )
		};

		let (mut tokens, ndq) = tkm::tokenise_dq( &pbuf, "{}:,[] \t\n", '\\', '"', true, dq );

		dt = match tokens.pop() {			// last token could be "short", must hold it
			Some( s ) => s,
			_ => String::from( "" )
		};

		dq = ndq;								// save for next buffer parse
		partial_tok = Option::Some( &dt );

		for mut t in tokens {

			let mut tt = type_token( &t );
			if tt == TType::Nil {						// treat nil tokens as ""
				tt = TType::String;
				t = String::from( "" );
			}

			if tt != TType::WhiteSpace {				// just skip whitespace
				//eprintln!( "parsing: {}", t );
				if ! next.is_expected( &tt ) {			// abort if not expeccted token
					eprintln!( "token '{}' (type: {}) is not expected; current state = {}", t, tt, state );
					eprintln!( "current name path: {}", path );
					break;
				}

				match tt {
					TType::OpenSq => {
						// when building an object (need value) the array is already created so that compiler does not complain about it being moved
						// we only need to create a new array thing when we are nesting an array
						if state == State::BuildArray {
							astack.push( at );				// push the  current array
							at = Arraything::mk();			// new array thing to start to populate
							//println!( "building sub array" );
						}
						push_sstack( state, &mut sstack );
						state =  State::BuildArray; 
						next = Expect::mk( vec![ TType::String, TType::Number, TType::Comma, TType::OpenSq, TType::CloseSq, TType::OpenCurly ] );
					},

					TType::OpenCurly => {
						if state == State::BuildArray {			// object in array needs a unique name
							let idx = at.pidx_str();			// next index as a string
							path.push( idx );					// add to path
						}

						astack.push( at );						// push the  current array
						at = Arraything::mk();					// need a new, already created, array at object init time
						push_sstack( state, &mut sstack );
						state = State::BuildObject;
						next = Expect::mk( vec![ TType::String, TType::Comma, TType::CloseCurly ] );
					},

					TType::CloseSq => {
						state = pop_stack( &mut sstack );
						if state == State::BuildArray {				// nested
							//println!( "closing sub array" );
							let fat = at;							// the one we just finished
							at = pop_astack( &mut astack );			// pop the old one
							at.push( Thing::Array( fat ) );			// and add the finished one to it
						} else {
							jl.add_thing( &path.to_str(), Thing::Array( at ) );
							at = Arraything::mk();					// MUST create a new one here, not when we encounter [

							if state == State::BuildObject {
								path.pop();
								//println!( " array closed; name popped: {}", path );
							}
						}

						next = Expect::mk( vec![  TType::Comma, TType::CloseCurly, TType::CloseSq ] );
					},

					TType::CloseCurly => {
						state = pop_stack( &mut sstack );
						//let _ignore = pop_astack( &mut astack );			// object will have "unused" array at end; must trash
						at = pop_astack( &mut astack );						// object will have "unused" array at end; must trash and get good at back

						next = match state {
							State::BuildObject => {
								path.pop();
								//println!( " obj closed; name popped: {}", path );
								Expect::mk( vec![  TType::Comma, TType::CloseCurly, TType::CloseSq ] )
							},

							State::BuildArray => {									// object is array element, must push
								at.push( Thing::Object( path.to_str() ) );			// and add the finished one to it
								path.pop();
								Expect::mk( vec![  TType::Comma, TType::CloseCurly, TType::CloseSq, TType::OpenCurly ] )
							}

							_ => Expect::mk( vec![  TType::Comma, TType::CloseCurly, TType::CloseSq ] )
						};
					},

					TType::Colon => {
						// there is no reason to do anything since the colon is useless
						next = Expect::mk( vec![ TType::OpenSq, TType::OpenCurly, TType::String, TType::Number ] );
					},

					TType::Comma => {
						next = match state {
							State::BuildArray =>  Expect::mk( vec![ TType::OpenCurly, TType::OpenSq, TType::String, TType::Number ] ),
							State::BuildObject => Expect::mk( vec![ TType::String ] ),
							_ => {
								eprintln!( "[FAIL] unexpected comma for state: {}", state );
								break; 
							}
						}
					},

					TType::String => {
						next = match state {
							State::BuildObject => {
								push_sstack( state, &mut sstack );
								state = State::NeedValue;
								path.push( t );
								
								Expect::mk( vec![ TType::Colon ] )
							},
							State::BuildArray => {
								//println!( "push string into array: {} {}", path, t );
								at.push( Thing::Str( t ) );

								Expect::mk( vec![ TType::CloseSq, TType::Comma ] )
							},
							State::NeedValue => {
								state = pop_stack( &mut sstack );
								jl.add_thing( &path.to_str(), Thing::Str( t ) );
								path.pop();

								Expect::mk( vec![ TType::CloseCurly, TType::Comma ] )
							},
							_ => {
								eprintln!( "[FAIL] unexpected string value '{}' for state: {}", t, state );
								break; 
							}
						}
					},	

					TType::Number => {
						next = match state {
							State::BuildArray => {
								//println!( "push number into array: {} {}", path, t );
								at.push( Thing::Float( atof64( &t ) ) );
								Expect::mk( vec![ TType::CloseSq, TType::Comma ] )
							},
							State::NeedValue => {
								state = pop_stack( &mut sstack );
								jl.add_thing( &path.to_str(), Thing::Float( atof64( &t ) ) );
								path.pop();

								Expect::mk( vec![ TType::CloseCurly, TType::Comma ] )
							},
							_ => {
								eprintln!( "[FAIL] unexpected number '{}' for state: {}", t, state );
								break; 
							}
						}
					},

					_=> ()				// white space (all that is left) is ignored
				};
			}
		}
	}

	return jl;
}


// -----------------------------------------------------------------------------------------------
/*
	Given a json file on the command line, parse it and print some information.
	If a second parameter is given on the command line, then that is assumed to
	be an array index and the contents of that elemnt are printed. The array
	is assumed to have the field name "states" unless a third parameter is 
	given which will is assumed to be the field name of the array.

	While the parser is flexible, this main is pretty rigid.
*/
fn main( ) {
	let mut fname = "test.json";
	let mut field = "[root].states";
	let mut idx = 12;

	let args: Vec< String > = std::env::args().collect();
	if args.len() > 1 {
		fname = &args[1];
		println!( "parsing: {}", fname );
	}
	if args.len() > 2 {
		idx =  atoi32( &args[2] );
	}
	if args.len() > 3 {
		field = &args[3];
	}

    let f = match File::open( fname ) {
		Ok( f ) => f,
		Err( e ) => {
			eprintln!( "unable to open file: {}: {}", fname, e );
			return;
		}
	};

	let jl = parse( f );

	if idx >= 0 {
		match jl.get_ele( field, idx as usize ) {
			Thing::Array( a ) => {
				println!( "[{}] array: {}", idx, a );
			},
			Thing::Str( s ) => {
				println!( "[{}] string: {}", idx, s );
			},
			Thing::Float( f ) => {
				println!( "[{}] float: {}", idx, f );
			},
			Thing::Object( o ) => {
				println!( "[{}] object: {}", idx, o );
			},
			_ =>  {
				println!( "[{}] Nil", idx );
			}
		};
	} else {
		let t = jl.get_thing( field );
		println!( "{} = {}", field, t );
	}

	jl.dump_keys();	
}
