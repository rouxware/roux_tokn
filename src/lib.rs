/*
	Date:	30 December 2020
	Author:	E. Scott Daniels
*/

/*!
	The roux_tokn crate provides a seris of tokenisers which go beyond
	the simple string splitting functions typically found in standard
	language runtime libraries.

	The tokenisers are provided in three modules:
	  * basic -- a basic tokeniser 
	  * quoted -- a tokeniser allowing for quoted tokens to avoid having to escape separater characters.
	  * multi -- a tokenser which allows for multiple separator characters.

	In addition to the tokenisers, there is a variable expansioin module, `vexpand,` that provides the ability
	to expand variables as the input buffers are parsed.
	as it is tokenised.
*/

pub mod basic;
pub mod quoted;
pub mod multi;
pub mod vexpand;

// ------------------ only tests below ---------------------------------------------
mod test_basic;		// test modules are set to private
mod test_multi;
mod test_quote;
mod test_vexp;


/*
	Global test support functions.
*/
#[cfg(test)]
mod tests {
	/*
		Build a symtab with n entries which have the key of varX and the value of
		"expanded X".
	*/
	pub( in crate ) fn build_symtab( n: i32 ) -> std::collections::HashMap<String, String> {
		let mut symtab: std::collections::HashMap<String, String> = std::collections::HashMap::new();
		for i in 0..n {
			let key =  format!( "var{}", i );
			let val =  format!( "expanded {}", i );
			symtab.insert( String::from( key ), String::from( val ) );
		}

		return symtab;
	}
}
