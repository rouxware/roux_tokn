
/*
	Date:		30 December 2020
	Author:		E. Scott Daniels
*/

/*!
	The multi module provides functions to tokenise strings using
	multiple sparators (e.g.  comma **or** vertical bar) while resepcting quoted
	portions of the input buffer; unescaped quotes are removed
	during parsing.
*/


/*	Programmer note:
	It might be possilbe to optimise this.
*/
/**
	The tokenise function parses the buffer *buf* splitting it into tokens
	governed by the separater list in the *sep* string. For
	example, if the seperator string is ",|", then both commas
	and vertical bars (pipe symbols) act as separaters. Quotes
	are respected using the value of the quote character parameter
	*qch* allowing the caller to set the desired quote mark.
	If *keep* is true, then the separater character is added
	as a unique token in the output.

	The result is a vector of strings; each elment is a token
	or a separater if *keep* was set to true.

	Note that unlike the other tokenisers in the crate, this function
	does **not** use a control string because it  must accept a string of multiple
	separator characters, and adding the escape and quote characters to the
	string was thought to be confusing.

	# Parameters

	Input paramters are:
	  * buf -- the address of the string to tokenise
	  * sep -- the address of the string containing one or more separator characters
	  * ech -- the character used to escape quote marks and separator characters
	  * qch -- the character used as the opening and closing quote (usually ' or ")
	  * keep -- a boolian flag that if true causes each separator encountered to be added as a token in the vector


	# Examples
	The first example illustrates how a Json-like string can be broken into tokens
	based on verious separators such as open and close curly braces. The example
	sets the *keep* option to `true` which preserves the separator(s) in the token
	list allowing  for a second parser to recognise things like *object start.*

	```
		use roux_tokn::multi as tokn;

		let buf = "{ \"title\": \"Stand Up and Cheer!\", \"performer\": \"The Ohio University Marching 110\" }";

		let tokens = tokn::tokenise( buf, "{},: ", '\\', '"', true );		// split on "json" separators
		let expect = vec![ "{", " ", "title", ":", " ", "Stand Up and Cheer!", ",", " ", "performer", 
				":", " ", "The Ohio University Marching 110", " ", "}" ];

		let mut errs = 0;
		for i in 0..tokens.len() {
			if expect.len() > i {
				if tokens[i] != expect[i] {
					eprintln!( "<TEST> error at token [{}] expected='{}' got='{}'  ", i, expect[i], tokens[i] );
					errs += 1;
				}
			}
		}

		eprintln!( "<TEST> there were {} errors", errs );
		# assert!( errs == 0 );
	```

	The second example uses the same input string, but parses the buffer with the 
	*keep* option set to false.  The resulting set of tokens thus does not include
	any of the separators.

	```
		use roux_tokn::multi as tokn;

		let buf = "{ \"title\": \"Stand Up and Cheer!\", \"performer\": \"The Ohio University Marching 110\" }";

		let tokens = tokn::tokenise( buf, "{},: ", '\\', '"', false );		// split on "json" separators
		let expect = vec![ "title", "Stand Up and Cheer!", "performer", "The Ohio University Marching 110", ];

		let mut errs = 0;
		for i in 0..tokens.len() {
			if expect.len() > i {
				if tokens[i] != expect[i] {
					eprintln!( "<TEST> error at token [{}] expected='{}' got='{}'  ", i, expect[i], tokens[i] );
					errs += 1;
				}
			}
		}

		eprintln!( "<TEST> there were {} errors", errs );
		# assert!( errs == 0 );
	```
*/
pub fn tokenise( buf: &str, sep: &str, ech: char, qch: char, keep: bool ) -> Vec<String> {
	let (tokens, _) = parse( buf, sep, ech, qch, keep, false );		// since we don't return dangling state, always assume false
	return tokens;
}

/**
	The `tokenise_dq() function parses the string into tokens returning a vector of string
	tokens and a "dangling flag". 
	The dangling flag is a booliean flag that when true indicates that the final token
	in the vector is incomplete. An incomplete token is one that started as a quoted
	token, but no quote was found to "close" token. 

	# Parameters
	Input paramters are:
	  * buf -- the address of the string to tokenise
	  * sep -- the address of the string containing one or more separator characters
	  * ech -- the character used to escape quote marks and separator characters
	  * qch -- the character used as the opening and closing quote (usually ' or ")
	  * keep -- a boolian flag that if true causes each separator encountered to be added as a token in the vector
	  * is_dangling -- a boolian flag that indicates the start of the buffer is the continuation of a quoted token

*/
pub fn tokenise_dq( buf: &str, sep: &str, ech: char, qch: char, keep: bool, is_dangling: bool ) -> (Vec<String>, bool) {
	return parse( buf, sep, ech, qch, keep, is_dangling );
}


/*
	This is the real workhorse of the module. The return values are the vector of
	token strings and a boolean flag to indicete if the last token is the product
	of a dangling (unclosed) quote; true if it is.

	In a multi sep world there is no such thing as a nil token. However "" should generate a nil
	token, but the problem is exactly what to generate.  Generating " " could be taken as whitespace
	and ignored.  So, we gnerate "<esc>nil" as the token where <esc> is the user's escape character.
	
*/
fn parse( buf: &str, sep: &str, ech: char, qch: char, keep: bool, is_dangling: bool ) -> (Vec<String>, bool) {
	let sv: Vec<char> = String::from( sep ).chars().collect();		// sep vector
    let mut sephash:  std::collections::HashMap<char,bool> = std::collections::HashMap::new();

	for s in sv {
		sephash.insert( s, true );
	}

	let wbuf: Vec<char> = String::from( buf ).chars().collect();

	let mut rv: Vec<String> = Vec::new();		// return vector of tokens
	let mut tok: Vec<char> = Vec::new();		// current token being built
	let mut force = false;						// set to true if we blindly put the next char in w/o checking
	let mut qip = is_dangling;					// start quoted if caller indicates such a state
	let mut was_quoted = qip;					// if we encounter "" then we return a nil token, so we must know if empty tok was quoted

	for c in wbuf {
		if qip {								// when in the middle of a quote
			if force {							// uncheced character forced in
				if c != qch {					// we drop escape only if quoting a quote ch
					tok.push( ech );
				}
				tok.push( c );
				force = false;
				continue;
			}

			if c == qch {					// ending quote, just reset and ignore
				qip = false;
				continue
			}

			if c == ech {
				force = true;
				continue;
			}

			tok.push( c );
			continue;
		}

		if force {
			tok.push( c );
			force = false;
			continue;
		}

		if c == qch {
			was_quoted = true;
			qip = true;					// open quote -- set flag and skip
			continue;
		}

		if c == ech {				// esacpe; force next in regardless
			force = true;
			continue;
		}

		if sephash.contains_key( &c ) {
			if tok.len() > 0 {
				let ts: String = tok.into_iter().collect();
				rv.push( ts );
			} else {
				if was_quoted {
					let mut nt = String::from( "" );
					nt.push( ech );
					nt.push_str( "nil" );		// special nil token: <esc>nil
					rv.push( nt );
				}
			}

			was_quoted = false;
			tok = Vec::new();
			if keep {
				rv.push( String::from( c ) );	
			}
			continue;
		}

		tok.push( c );
	}

	if tok.len() > 0 {						// snarf last token
		let ts: String = tok.into_iter().collect();
		rv.push( ts );
	} else {
		if was_quoted {
			let mut nt = String::from( "" );
			nt.push( ech );
			nt.push_str( "nil" );		// special nil token: <esc>nil
			rv.push( nt );
		}
	}

	return (rv, qip);
}
