
/*
	Date:		30 December 2020
	Author:		E. Scott Daniels
*/

/*!
	The quoted module contains functions that provide tokenising
	while respecting separators embedded in quoted portions of
	the buffer.
*/


/**
	The tokenise function parses the string (buf) separating it
	into tokens based on the separater in the control string.

	The control string (ctl) is a series of three characters:

	- The separator character
	- The escape character
	- The quote character

	The last two characters are optional and when omitted default
	to a backslant (\) and double quote (") respectively.
	The following control string sets the separator as the vertical
	bar character (|), the escape character as the carrot (^) and
	the quote character as a single quote:

	```text
		|^'
	```

	The return is a vector of String with each element being a logical
	token from the buffer.

	Note that multiple adjacent separators will result in empty tokens.
	This is generally the expected behaviour when parsing pipe or comma
	separated tokens (e.g  foo,,bar should yeield an empty token in between).
	However, when parsing plain text, where the space character is the
	separator, the generally expected behaviour is to treat all adjacent
	separators as one and is **not** the default behaviour of the tokenise
	function.

	# Example

	```
		use roux_tokn as tokn;

		let buf = "Now is the time for all 'quoted tokens' to be kept together. That^'s right!";
		let tokens = tokn::quoted::tokenise( buf, " ^'" );			// split on space, use ^ as escape with single quotes
		let expect = vec![ "Now", "is", "the", "time", "for", "all", "quoted tokens",
			"to", "be", "kept", "together.", "That's", "right!" ];

		let mut errs = 0;
		for i in 0..tokens.len() {
			if expect.len() > i {
				if tokens[i] != expect[i] {
					eprintln!( "<TEST1> error at token [{}] expected='{}' got='{}'  ", i, expect[i], tokens[i] );
					errs += 1;
				}
			}
		}

		eprintln!( "<TEST> there were {} errors", errs );
		# assert!( errs == 0 );
	```
*/
pub fn tokenise( buf: &str, ctl: &str ) -> Vec< String > {
	let (tokens, _) = parse( buf, ctl, false );
	return tokens;
}

/**
	This parser is exactly the same as the `tokenise()` parser except that the return
	includes a boolian set to *true* if the last token is the result of a 
	*dangling,* (i.e. unclosed) quote.

	A dangling quote isn't always an error and thus this function does not treat
	it as such. In the case where multiple blocks of input are parsed in a 
	streaming manner, it would be expected that some blocks end with a dangling
	quoteed token and that the calling function will take the correct action based
	on the return boolean value.

	# Example
	This example illustrates how to use the parser with dangling quote detection.
	For this example the single quote (') is used to avoid escapes which might make
	the code more difficult to read, and the escape character is a carrot (^) for
	the same reason.

	```
		use roux_tokn as tokn;

		let buf = "Now is the time for all 'quoted tokens' to be kept together. 'That^'s right!";
		let (tokens, dq) = tokn::quoted::tokenise_dq( buf, " ^'" );			// split with single quotes as the quote mark
		let expect = vec![ "Now", "is", "the", "time", "for", "all", "quoted tokens",
			"to", "be", "kept", "together.", "That's right!" ];

		let mut errs = 0;
		for i in 0..tokens.len() {
			if expect.len() > i {
				if tokens[i] != expect[i] {
					eprintln!( "<TEST1> error at token [{}] expected='{}' got='{}'  ", i, expect[i], tokens[i] );
					errs += 1;
				}
			}
		}

		if dq {
			eprintln!( "the input had a dangling quote!" );	// this is expected
			# assert!( dq, "dangling quote flag wasn't set" );
		}
			
		eprintln!( "<TEST> there were {} errors", errs );
		# assert!( errs == 0 );
	```
*/
pub fn tokenise_dq( buf: &str, ctl: &str ) -> (Vec< String >, bool ) {
	return parse( buf, ctl, false );
}


/**
	The tokenise_ws function provides an easy way to parse "text" into
	tokens with the assumption that separators are white space (spaces, tabs
	newlines or returns), and that multiple adjacent separators are to be treated
 	as a single separator.

	The control string (ctl) is used to provide the escape character and the quote
	character. If an empty string is supplied, then the escape character is assumed
	to be a backslant (\) and the quote character assumed to be a double quote (").

	The return value is a vector of strings which are the tokens.

	# Example

	```
		use roux_tokn as tokn;

		// Extra spaces should be treated as a single separator and the lead tab ignored.
		let buf = "\tNow  is   the   time for all \n'quoted tokens' to be kept together. That^'s right!";
		let tokens = tokn::quoted::tokenise_ws( buf, "^'" );			// split on white space with ^ as esc

		let expect = vec![ "Now", "is", "the", "time", "for", "all", "quoted tokens",
			"to", "be", "kept", "together.", "That's", "right!" ];

		let mut errs = 0;
		for i in 0..tokens.len() {
			if expect.len() > i {
				if tokens[i] != expect[i] {
					eprintln!( "<TEST> error at token [{}] expected='{}' got='{}'  ", i, expect[i], tokens[i] );
					errs += 1;
				}
			}
		}

		eprintln!( "<TEST> tokenising with whitespace ignored, there were {} errors", errs );
		# assert!( errs == 0 );
	```
*/
pub fn tokenise_ws( buf: &str, ctl: &str ) -> Vec< String > {
	let xctl = String::from( " " );
	let (tokens, _) =  parse( buf, &(xctl + ctl), true );
	return tokens;
}

/**
	The tokenise_wsdq parser is exactly the same as the `tokenise_ws()` parser except that the return
	includes a boolian set to *true* if the last token is the result of a 
	*dangling,* (i.e. unclosed) quote.

	A dangling quote isn't always an error and thus this function does not treat
	it as such. In the case where multiple blocks of input are parsed in a 
	streaming manner, it would be expected that some blocks end with a dangling
	quoteed token and that the calling function will take the correct action based
	on the return boolean value.

	# Example
	This example illustrates how to use the parser with dangling quote detection.
	For this example the single quote (') is used to avoid escapes which might make
	the code more difficult to read, and the escape character is a carrot (^) for
	the same reason.

	```
		use roux_tokn as tokn;

		let buf = "\tNow  is   the   time for all \n'quoted tokens' to be kept together. 'That^'s right!";
		let (tokens, dq) = tokn::quoted::tokenise_wsdq( buf, "^'" );
		let expect = vec![ "Now", "is", "the", "time", "for", "all", "quoted tokens",
			"to", "be", "kept", "together.", "That's right!" ];

		let mut errs = 0;
		for i in 0..tokens.len() {
			if expect.len() > i {
				if tokens[i] != expect[i] {
					eprintln!( "<TEST1> error at token [{}] expected='{}' got='{}'  ", i, expect[i], tokens[i] );
					errs += 1;
				}
			}
		}

		if dq {
			eprintln!( "the input had a dangling quote!" );	// this is expected
			# assert!( dq, "dangling quote flag wasn't set" );
		}
			
		eprintln!( "<TEST> there were {} errors", errs );
		# assert!( errs == 0 );
	```
*/
pub fn tokenise_wsdq( buf: &str, ctl: &str ) -> (Vec< String >, bool ) {
	let xctl = String::from( " " );
	return parse( buf, &(xctl + ctl), true );
}


/*
	Looks to see if the ch passed in is the sep char; if this is a
	ws_split parse (ws is true), then sch is ignored and true is
	returned if the ch is whitespace.
*/
fn is_sep( ch: char, sch: char, ws: bool ) -> bool {
	if ws {
		return ch == ' ' || ch == '\t' || ch == '\n'
	}

	return ch == sch;
}

/*
	This is the real workhorse of the module.
	The return value is the vector of token strings and a boolean set to true
	if the last token is in complete because there was a quote in progress.
*/
fn parse( buf: &str, ctl: &str, ws_split: bool ) -> ( Vec<String>, bool ) {
	let cv: Vec<char> = String::from( ctl ).chars().collect();
	let sch = cv[0];				// seperator character
	let mut ech: char  = '\\';		// default escape
	let mut qch: char  = '"';		// default quote
	if cv.len() > 1 {
		ech = cv[1];
	}
	if cv.len() > 2 {
		qch = cv[2];
	}

	let wbuf: Vec<char> = String::from( buf ).chars().collect();

	let mut rv: Vec<String> = Vec::new();		// return vector of tokens
	let mut tok: Vec<char> = Vec::new();		// current token being built
	let mut force = false;						// set to true if we blindly put the next char in w/o checking
	let mut qip = false;						// quoted portion of the token in progress

	for c in wbuf {
		if qip {								// when in the middle of a quote
			if force {							// uncheced character forced in
				if c != qch {					// we drop the escape only if a quote was escaped (something like \n stays)
					tok.push( ech );
				}
				tok.push( c );
				force = false;
				continue;
			}

			if c == qch {					// ending quote, just reset and ignore
				qip = false;
				continue
			}

			if c == ech {
				force = true;
				continue;
			}

			tok.push( c );
            continue;
		}

        if force {
            if !is_sep( c, sch, ws_split )  &&  c != qch  {
                tok.push( ech );		// we only drop the escape for a seperator or leading quote
            }
            tok.push( c );
            force = false;
            continue;
        }

        if c == qch {
            qip = true;					// open quote -- set flag and skip
            continue;
        }

        if c == ech {				// escape; force next in regardless
            force = true;
            continue;
        }

       	if is_sep( c, sch, ws_split ) {
			if tok.len() > 0  || ! ws_split {
           		let ts: String = tok.into_iter().collect();
           		rv.push( ts );
           		tok = Vec::new();
			}
           	continue;
       	}

        tok.push( c );              // finally, no special handling just push it on
	}

	let ts: String = tok.into_iter().collect();     // if final ch is a sep this is empty and that's OK
	rv.push( ts );

	return (rv, qip);
}
