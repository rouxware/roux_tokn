/*
	Abstract:	These are the unit tests for the basic module.
	Date:		14 April 2021
	Author:		E. Scott Daniels
*/

#[cfg(test)]
mod tests {
	use crate::basic as basic;

    #[test]
    fn basic_simple() {
		let input = "Now is the time";
		let expect: Vec<&str> = input.split( " " ).collect();
		let tokens = basic::tokenise( input, " \\" );

		assert_eq!( tokens.len(), expect.len(), "number of tokens not correct; expected {} found {}", expect.len(), tokens.len() );
		for i in 0..tokens.len() {
			eprintln!( "<TEST> basic: [{}] {}", i, tokens[i] );
			if expect.len() > i {
				assert!( tokens[i] == expect[i], "token at [{}] did not match t={}", i, tokens[i] );
			}
		}
    }

    #[test]
    fn basic_esc() {
		let input = "Now is\\ the time";
		let tokens = basic::tokenise( input, " \\" );
		let expect = vec![ "Now", "is the", "time" ];

		for i in 0..tokens.len() {
			eprintln!( "<TEST> basic_esc: [{}] {}", i, tokens[i] );
			if expect.len() > i {
				assert!( tokens[i] == expect[i], "token at [{}] did not match t={}", i, tokens[i] );
			}
		}
		assert_eq!( tokens.len(), expect.len(), "number of tokens not correct; expected {} found {}", expect.len(), tokens.len() );
    }

	/*
		Test for nil last token
	*/
    #[test]
    fn basic_nil1() {
		let input = "Now is\\ the time ";
		let tokens = basic::tokenise( input, " \\" );
		let expect = vec![ "Now", "is the", "time", "" ];

		for i in 0..tokens.len() {
			eprintln!( "<TEST> basic_nil: [{}] {}", i, tokens[i] );
			if expect.len() > i {
				assert!( tokens[i] == expect[i], "nil: token at [{}] did not match t={}", i, tokens[i] );
			}
		}
		assert_eq!( tokens.len(), expect.len(), "nil: number of tokens not correct; expected {} found {}", expect.len(), tokens.len() );
    }

	/*
		Test to ensure that basic parser generates a nil token when there
		are multiple separaters adjacent to each other.
	*/
    #[test]
    fn basic_nil2() {
		let input = "Now is\\ the  time";					// generates a nil token between the and time
		let tokens = basic::tokenise( input, " \\" );
		let expect = vec![ "Now", "is the", "", "time" ];

		for i in 0..tokens.len() {
			eprintln!( "<TEST> basic_nil2: [{}] {}", i, tokens[i] );
			if expect.len() > i {
				assert!( tokens[i] == expect[i], "nil2: token at [{}] did not match t={}", i, tokens[i] );
			}
		}
		assert_eq!( tokens.len(), expect.len(), "nil2: number of tokens not correct; expected {} found {}", expect.len(), tokens.len() );
    }

	/*
		Test to ensure that multiple adj separators don't produce
		a nil token.
	*/
    #[test]
    fn basic_nonil() {
		let input = "Now is  the  time";							// generates a nil token between the and time
		let tokens = basic::tokenise_skip( input, " \\" );		// the nil skip parser
		let expect = vec![ "Now", "is", "the", "time" ];

		for i in 0..tokens.len() {
			eprintln!( "<TEST> basic_nonil: [{}] {}", i, tokens[i] );
			if expect.len() > i {
				assert!( tokens[i] == expect[i], "nonil: token at [{}] did not match t={}", i, tokens[i] );
			}
		}
		assert_eq!( tokens.len(), expect.len(), "number of tokens not correct; expected {} found {}", expect.len(), tokens.len() );
    }

    #[test]
    fn basic_comma() {
		let input = "Now,is the,time";
		let expect: Vec<&str> = input.split( "," ).collect();
		let tokens = basic::tokenise( input, ",\\" );

		assert_eq!( tokens.len(), expect.len(), "number of tokens not correct; expected {} found {}", expect.len(), tokens.len() );
		for i in 0..tokens.len() {
			eprintln!( "<TEST> basic_comma: [{}] {}", i, tokens[i] );
			if expect.len() > i {
				assert!( tokens[i] == expect[i], "token at [{}] did not match t={}", i, tokens[i] );
			}
		}
    }
}
