/*
	Abstract:	These are the unit tests for the quoted module.
	Date:		14 April 2021 (broken out of lib.rs)
	Author:		E. Scott Daniels
*/

#[cfg(test)]
mod tests {
	use crate::quoted as quoted;

    #[test]
    fn quoted() {
		let input = "Now \"is the\" time";
		let expect = vec![ "Now", "is the", "time" ];

		let tokens = quoted::tokenise( input, " \\" );

		assert_eq!( tokens.len(), expect.len(), "number of tokens not correct; expected {} found {}", expect.len(), tokens.len() );
		for i in 0..tokens.len() {
			eprintln!( "<TEST> basic: [{}] {}", i, tokens[i] );
			if expect.len() > i {
				assert!( tokens[i] == expect[i], "token at [{}] did not match t={}", i, tokens[i] );
			}
		}
    }

    #[test]
    fn quoted_alt() {
		let input = "Now 'is the' time";
		let tokens = quoted::tokenise( input, " \\'" );
		let expect = vec![ "Now", "is the", "time" ];

		for i in 0..tokens.len() {
			eprintln!( "<TEST> quoted-alt: [{}] {}", i, tokens[i] );
			if expect.len() > i {
				assert!( tokens[i] == expect[i], "token at [{}] did not match t={}", i, tokens[i] );
			}
		}
		assert_eq!( tokens.len(), expect.len(), "number of tokens in string with single quotes not expected; wanted {} found {}", expect.len(), tokens.len() );
    }

	/*
		Quoted processing with various escapes.
	*/
    #[test]
    fn quoted_esc() {
		let input = "Now 'is \\'the\\'\\n' time\\n";				// the \' in the quotes should drop the esc, but not for \n either inside or out
		let tokens = quoted::tokenise( input, " \\'" );
		let expect = vec![ "Now", "is 'the'\\n", "time\\n" ];

		for i in 0..tokens.len() {
			eprintln!( "<TEST> quoted-esc: [{}] {}", i, tokens[i] );
			if expect.len() > i {
				assert!( tokens[i] == expect[i], "token at [{}] did not match t={}", i, tokens[i] );
			}
		}
		assert_eq!( tokens.len(), expect.len(), "number of tokens in string with single quotes not expected; wanted {} found {}", expect.len(), tokens.len() );
    }

	/*
		Validate that <chars><quote><stuff><quote> produces the leading chars with
		the right stuff which should contain separator characters.
	*/
    #[test]
    fn quoted_cat() {
		let input = "Now is'   the 'time for all good climbers";		// "is   the time" should be treated as a single token with extra spaces
		let tokens = quoted::tokenise( input, " \\'" );
		let expect = vec![ "Now", "is   the time", "for", "all", "good", "climbers" ];

		for i in 0..tokens.len() {
			eprintln!( "<TEST> quoted-cat: [{}] {}", i, tokens[i] );
			if expect.len() > i {
				assert!( tokens[i] == expect[i], "token at [{}] did not match t=({}) expected=({})", i, tokens[i], expect[i] );
			}
		}
		assert!( tokens.len() == expect.len(), "number of tokens didn't match expected: {} != {}", tokens.len(), expect.len() );
    }

	/*
		Ensure that a string witih a dangling quote returns with the dq flag set.
	*/
	#[test]
	fn quoted_dq( ) {
		let input = "Standing up to cheer is what all good 'Ohio University students do";
		let (tokens, dq) = quoted::tokenise_dq( input, " ^'" );
		let expect = vec![ "Standing", "up", "to", "cheer", "is", "what", "all", "good", "Ohio University students do" ];

		for i in 0..tokens.len() {
			eprintln!( "<TEST> quoted-cat: [{}] {}", i, tokens[i] );
			if expect.len() > i {
				assert!( tokens[i] == expect[i], "token at [{}] did not match t=({}) expected=({})", i, tokens[i], expect[i] );
			}
		}
		assert!( tokens.len() == expect.len(), "number of tokens didn't match expected: {} != {}", tokens.len(), expect.len() );
		assert!( dq, "dangling quote flag was NOT set when expected" );
    }

	/*
		Test that multiple white space is treated as a single separator,
		but only when outside of quotes. The multiple spaces between the
		sords "is" and "the time" in the input should remain in the output.
		All others should be treated as a single separator.
	*/
    #[test]
    fn quoted_ws() {
		let input = "Now is'   the 'time for all    good \n\t climbers";	// the multiple spaces between 'is' and 'the' should remain
		let tokens = quoted::tokenise_ws( input, "\\'" );
		let expect = vec![ "Now", "is   the time", "for", "all", "good", "climbers" ];

		for i in 0..tokens.len() {
			eprintln!( ">>>[{}] = '{}'", i, tokens[i] );
		}
		for i in 0..tokens.len() {
			eprintln!( "<TEST> quoted-cat: [{}] {}", i, tokens[i] );
			if expect.len() > i {
				assert!( tokens[i] == expect[i], "token at [{}] did not match t=({}) expected=({})", i, tokens[i], expect[i] );
			}
		}
		assert!( tokens.len() == expect.len(), "number of tokens didn't match expected: {} != {}", tokens.len(), expect.len() );
    }

	/*
		Test that multiple white space is treated as a single separator and that
		a dangling quoteed token is correctly identified.,
	*/
    #[test]
    fn quoted_wsdq() {
		let input = "Now is'   the 'time for all    good \n\t 'climbers";	// dangling quote
		let (tokens, dq) = quoted::tokenise_wsdq( input, "\\'" );
		let expect = vec![ "Now", "is   the time", "for", "all", "good", "climbers" ];

		for i in 0..tokens.len() {
			eprintln!( ">>>[{}] = '{}'", i, tokens[i] );
		}
		for i in 0..tokens.len() {
			eprintln!( "<TEST> quoted-cat: [{}] {}", i, tokens[i] );
			if expect.len() > i {
				assert!( tokens[i] == expect[i], "token at [{}] did not match t=({}) expected=({})", i, tokens[i], expect[i] );
			}
		}
		assert!( tokens.len() == expect.len(), "number of tokens didn't match expected: {} != {}", tokens.len(), expect.len() );
		assert!( dq, "dangling quote flag was not set when it should have been" )
    }

	/*
		Test that multiple white space is treated as a single separator and that
		the dangling quote flag is false.
	*/
    #[test]
    fn quoted_ws_nodq() {
		let input = "Now is'   the 'time for all    good \n\t climbers";	// dangling quote
		let (tokens, dq) = quoted::tokenise_wsdq( input, "\\'" );
		let expect = vec![ "Now", "is   the time", "for", "all", "good", "climbers" ];

		for i in 0..tokens.len() {
			eprintln!( ">>>[{}] = '{}'", i, tokens[i] );
		}
		for i in 0..tokens.len() {
			eprintln!( "<TEST> quoted-cat: [{}] {}", i, tokens[i] );
			if expect.len() > i {
				assert!( tokens[i] == expect[i], "token at [{}] did not match t=({}) expected=({})", i, tokens[i], expect[i] );
			}
		}
		assert!( tokens.len() == expect.len(), "number of tokens didn't match expected: {} != {}", tokens.len(), expect.len() );
		assert!( !dq, "dangling quote flag was set when it should NOT have been" )
    }
}
