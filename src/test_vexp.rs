/*
	Abstract:	These are the unit tests for the variable expansion module.
	Date:		14 April 2021	(broken out of lib.rs)
	Author:		E. Scott Daniels
*/

#[cfg(test)]
mod tests {
	use crate::vexpand as vexpand;
	use crate::tests;

	#[test]
	fn vexpand_basic() {
		let symtab: std::collections::HashMap<String, String> = tests::build_symtab( 4 );

		let estr  = vexpand::parse( "now is the time for ${var1} to be lead by ${var2!foo bar} to the edge of ${var10!some unknown place}", symtab, '$', '\\' );
		let expect = "now is the time for expanded 1 to be lead by expanded 2 to the edge of some unknown place";

		eprintln!( "<TEST> vexpand_basic: got:      {}", estr );
		eprintln!( "<TEST> vexpand_basic: expected: {}", expect );
		assert!( estr == expect, "incorrect basic var expansion" );
	}

	#[test]
	fn vexpand_esc() {
		let symtab: std::collections::HashMap<String, String> = tests::build_symtab( 4 );

		let estr = vexpand::parse( "now is the time for \\${var1} to be lead by 'this was quoted ${var2!foo bar}' to the edge of ${var10!some unknown place}", symtab, '$', '\\' );
		eprintln!( "<TEST> vexpand_esc: {}", estr );
		assert!( estr.contains( " ${var1}" ), "result did not contain intentionally unexpanded (escaped) var1 reference" );
		assert!( estr.contains( "${var2!foo bar}" ), "result did not contain intentionally unexpanded (quoted) var2 references in quotes" );
	}

	#[test]
	fn vexpand_adj() {
		let symtab: std::collections::HashMap<String, String> = tests::build_symtab( 4 );

		let estr  = vexpand::parse( "now is the time for \\'&{var1}&{var2!foo bar}\\' to the edge of &{var10!some unknown place}", symtab, '&', '\\' );
		eprintln!( "<TEST> vexpand_adj: {}", estr );

		assert!( estr.contains( "'expanded 1expanded 2'" ), "result did not contain single quotes round expanded vars" );
		assert!( estr.contains( "edge of some unknown place" ), "result did not contain the default for var10" );
	}

	#[test]
	fn vexpand_quote( ) {
		let symtab: std::collections::HashMap<String, String> = tests::build_symtab( 4 );
		
		let expect = "quotes in string (') remain but this string was 'quoted'\\n";
		let estr  = vexpand::parse( "quotes in string (\\') remain 'but this string was \\'quoted\\'\\n'", symtab, '$', '\\'  );
		eprintln!( "<TEST> vexpand_quote: {}", estr );

		assert!( expect == estr, "expanded string did not match expected" );
	}

	/*
		Specifically drives code that preserves escape symbols outside of quotes.
	*/
	#[test]
	fn vexpand_esc2( ) {
		let symtab: std::collections::HashMap<String, String> = tests::build_symtab( 4 );
		
		let orig = "The escapes ${var1} in this string should remain\\n";
		let expect = "The escapes expanded 1 in this string should remain\\n";
		let estr  = vexpand::parse( orig, symtab, '$', '\\'  );
		eprintln!( "<TEST> vexpand_quote: {}", estr );

		assert!( expect == estr, "expanded string did not match expected" );
	}
}

