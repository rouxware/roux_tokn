
/*
	Abstract:	A variable expsnsion tool.  The buffer is parsed for
				variables of the form:
					${name}
					${name!default}

				The var dereference character ($ in the example) is passed
				in along with the escape character.

				When encountered the symbol table passed in is used to
				expand the names and a buffer with the expansions is 
				returned. Variables are NOT expanded inside of single
				quotes unless the quotes are escaped. Escaped single
				quotes and escaped var symbols (e.g. $) are the only
				time the escape is removed. Single quotes are removed
				if unescaped.

	Date:		30 December 2020
	Author:		E. Scott Daniels
*/
/*!
	The variable expansion module provides functions which support the parsing
	of a buffer to expand variables using a user supplied symbol table.
*/

/**
	The variable expansion parser accepts a buffer and a symbol table (key value) 
	and replaces any references in the buffer to a symbol table key with the
	value.  

	References in the buffer are expected to be of the form:

	```text
		${name[!default]}
	```


	Where *name* is the key to be looked up in the symbol table. The default
	value is optional, and is used if the key is not defined. The dereference
	character (The $ in the example) is passed in to the parser allowing the
	user programme the most flexibility. 
		
	The expansion is **not** applied inside of quoted portions of the buffer
	("...") unless the quotes are escaped.  The dereferencing symbol may also
	be escaped to allow it to be included as is.  Like the dereferencing characer,
	the escape character may be passed in by the user programme.

	The return is a string with all variables expanded.
*/
pub fn parse( buf: &str, symtab: std::collections::HashMap<String, String>, vch: char, ech: char  ) -> String {

	let mut vname: Vec<char> = Vec::new();
	let mut def_exp: Vec<char> = Vec::new();
	let wbuf: Vec<char> = String::from( buf ).chars().collect();		// parsable buffer

	let mut ret_buf: Vec<char> = Vec::new();	// buffer for return
	let mut force = false;						// set to true if we blindly put the next char in w/o checking
	let mut qip = false;						// quoted portion of the token in progress
	let mut eip = false;						// expansion in progress
	let mut have_def = false;

	for c in wbuf {
		if qip {								// when in the middle of a quote
			if force {							// previous character was an escape; remove it only if this character is a single quote
				if c != '\'' {
					ret_buf.push( ech );
				}
				ret_buf.push( c );
				force = false;
				continue;
			}

			if c == '\'' {						// ending quote, just reset and ignore
				qip = false;
				continue
			}

			if c == '\\' {
				force = true;
				continue;
			}

			ret_buf.push( c );
			continue;
		}

		if eip {							// expansion in progress
			if c == '{' {
				have_def = false;			// no known default at this point
				continue;
			}

			if c == '!' {
				have_def = true;
				continue;
			}

			if c == '}' {
				let vn: String = vname.into_iter().collect();	// convert variable name to string for lookup
				vname = Vec::new();								// must reallocate here to keep compiler happy
				match symtab.get( &vn ) {						// attempt to look up the name
					Some( s ) =>  {
						let ebuf: Vec<char> = s.chars().collect();		// convert expansion string from symtab to char
						for ec in ebuf {								// push the expansion into the output
							ret_buf.push( ec );
						}
						def_exp = Vec::new();						// reset before next go
					},
					_ => {
						if have_def {
							for ec in def_exp {							// push the default into the output
								ret_buf.push( ec );
							}

							def_exp = Vec::new();						// must allocate here to keep compiler happy
						}
					}
				}

				eip = false;
				continue;
			}

			if have_def {				// add to vname or default expansion depending on state
				def_exp.push( c );
			} else {
				vname.push( c );
			}
			continue;
		}

		if force {
			if c != vch && c != '\'' {	// if anything other than the var sym or quote was escaped, keep the escape too
				ret_buf.push( ech )
			}
			ret_buf.push( c );
			force = false;
			continue;
		}

		if c == vch {						// start of a variable
			// to keep the compiler happy, vname is allocated when the end of previous var is found, so not done here
			eip = true;
			continue;
		}

		if c == '\'' {
			qip = true;						// open quote -- set flag and skip
			continue;
		}

		if c == ech {						// esacpe; possibly force next in without treating it specially
			force = true;
			continue;
		}

		ret_buf.push( c );					// no other state; just push the character into the output buffer
	}

	return ret_buf.into_iter().collect();		// combine chars into a string and push out
}
